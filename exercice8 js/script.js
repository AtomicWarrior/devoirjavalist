//Create a class that will contain every useful element of a task 
class Task
{
	constructor(name, quantity, price, checked)
	{
		this.name = name;
		this.quantity = quantity;
		this.price = price;
		this.checked = checked;
	}

}


//Initialize the tasklist
tabTasks = [];


//Functions

function AddToTheList()
{
	//Retrieve the data from the input fields
	name = document.getElementById("namebox").value;
	quantity = document.getElementById("quantitybox").value;
	price = document.getElementById("pricebox").value;

	//No need to check for wrong values because of the way the form is made

	//Add the retrieved text into the tasklist
	tabTasks.push(new Task(name, quantity, price, false));


	//Refresh the displayed values
	updateTaskList();

	
	//Prevent the browser page from refreshing
	return false;
}

function deleteTask(clicked_id)
{

	var deletedId = Number(clicked_id);

	console.log(deletedId);

	//Destroy the task in the array
	tabTasks.splice(deletedId, 1);
	
	//Debug
	console.log(tabTasks);

	updateTaskList();
}

function updateTaskList()
{
	//This function deletes all of the html nodes, and recreates new ones according to the stored array
	
	var list = document.querySelector("#mainList");
	
	//Destroy every node
	var child = list.lastElementChild;
	while(child)
	{
		list.removeChild(child);
		child = list.lastElementChild;
	}

	//Recreate every node necessary
	var theList = document.getElementById("mainList");

	var theFirstTitle = document.createElement("li");

	var theTitle1 = document.createElement("p");
	theTitle1.classList.add("titrecol");
	theTitle1.appendChild(document.createTextNode("Article"));
	var theTitle2 = document.createElement("p");
	theTitle2.classList.add("titrecol");
	theTitle2.appendChild(document.createTextNode("Quantité"));
	var theTitle3 = document.createElement("p");
	theTitle3.classList.add("titrecol");
	theTitle3.appendChild(document.createTextNode("Prix total"));

	theFirstTitle.appendChild(theTitle1);
	theFirstTitle.appendChild(theTitle2);
	theFirstTitle.appendChild(theTitle3);

	theList.appendChild(theFirstTitle);


	for(var i = 0; i < tabTasks.length; i++)
	{

		//Create nodes for the whole task
		var theLi = document.createElement("li");
		theLi.classList.add("row");

			//Name
		var theName = document.createElement("div");
		theName.classList.add("col-4");
		theName.appendChild(document.createTextNode(tabTasks[i].name));
			
			//Quantity
		var theQuantity = document.createElement("div");
		theQuantity.classList.add("col-6");
		var theQuantityBox = document.createElement("input");
		theQuantityBox.type = "number";
		theQuantityBox.min = "1";
		theQuantityBox.value = tabTasks[i].quantity;
		theQuantityBox.id = "quant"+i;
		theQuantityBox.onchange = function(){ changeQuantity(this.id) }
		theQuantity.appendChild(theQuantityBox);


			
			//Price
		var thePrice = document.createElement("div");
		thePrice.classList.add("col-2");
		thePrice.appendChild(document.createTextNode(tabTasks[i].price * tabTasks[i].quantity + "€"));
			
			//Check
		var theCheck = document.createElement("div");
		theCheck.classList.add("col-2");
		var theCheckChild = document.createElement("input");
		theCheckChild.type = "checkbox";
		theCheckChild.checked = tabTasks[i].checked;
		theCheckChild.id = "check"+i;
		theCheckChild.onchange = function(){ changeCheck(this.id) }
		theCheck.appendChild(theCheckChild);

			//Button component
		var theDiv = document.createElement("div");
		theDiv.classList.add("col-2");
		var theButton = document.createElement("button");
		theButton.id = i;
		theButton.classList.add("btn-danger");
		theButton.appendChild(document.createTextNode("Supprimer"));
		theButton.onclick = function(){	deleteTask(this.id)};
		theDiv.appendChild(theButton);
		
		//Add everything in the <li>
		theLi.appendChild(theName);
		theLi.appendChild(theQuantity);
		theLi.appendChild(thePrice);
		theLi.appendChild(theCheck);
		theLi.appendChild(theDiv);

			

			//Add it in the html
		
		theList.appendChild(theLi);
	}

	var totalPrice = 0;
	for(var j = 0; j < tabTasks.length; j++)
	{
		totalPrice = totalPrice + (tabTasks[j].quantity * tabTasks[j].price);
	}

	var theTotalPrice = document.createElement("li");
	theTotalPrice.appendChild(document.createTextNode("Prix total : "+totalPrice+"€"));
	theList.appendChild(theTotalPrice);


}

function changeCheck(id)
{
	var realId = id.substring(5);
	tabTasks[realId].checked = !tabTasks[realId].checked;
	
	console.log(tabTasks);

}

function changeQuantity(id)
{
	var realId = id.substring(5);
	tabTasks[realId].quantity = document.getElementById(id).value;
	//Refresh to update the whole values
	updateTaskList();

}